#!/usr/bin/env python3

# -------
# imports
# -------
from Netflix import netflix_eval, algo_prediction
from unittest import main, TestCase
from math import sqrt
from io import StringIO
from numpy import sqrt, square, mean, subtract

# -----------
# TestNetflix
# -----------

class TestNetflix (TestCase):

    # ----
    # eval
    # ----

    def test_eval_1(self):
        r = StringIO('1:\n30878\n2647871\n1283744\n')
        w = StringIO()
        netflix_eval(r, w)
        self.assertEqual(
            w.getvalue(), '1:\n3.77971003558\n3.37971003558\n3.68671003558\n0.5491\n')

    def test_eval_2(self):
        r = StringIO('10040:\n2417853\n1207062\n2487973\n')
        w = StringIO()
        netflix_eval(r, w)
        self.assertEqual(
            w.getvalue(), '10040:\n3.0687100355799997\n2.87671003558\n3.5627100355799994\n0.7804\n')

    def test_eval_3(self):
        r = StringIO('10:\n1952305\n1531863\n')
        w = StringIO()
        netflix_eval(r, w)
        self.assertEqual(
            w.getvalue(), '10:\n2.98571003558\n2.73171003558\n0.1899\n')

    def test_eval_4(self):
        r = StringIO('10004:\n1737087\n1270334\n1262711\n')
        w = StringIO()
        netflix_eval(r, w)
        self.assertEqual(
            w.getvalue(), '10004:\n5.1507100355799995\n4.17571003558\n4.39571003558\n0.9398\n')


    # ----
    # Prediction
    # ----

    def test_prediction_1(self):
        prediction = algo_prediction(30878, 1)
        self.assertEqual(prediction, 3.77971003558)

    def test_prediction_2(self):
        prediction = algo_prediction(1952305, 10)
        self.assertEqual(prediction, 2.98571003558)

    def test_prediction_3(self):
        prediction = algo_prediction(2326571, 1000)
        self.assertEqual(prediction, 3.27671003558)

    def test_prediction_4(self):
        prediction = algo_prediction(200206, 10000)
        self.assertEqual(prediction, 3.70371003558)



# ----
# main
# ----			
if __name__ == '__main__':
    main()

""" #pragma: no cover
% coverage3 run --branch TestNetflix.py >  TestNetflix.out 2>&1



% coverage3 report -m                   >> TestNetflix.out



% cat TestNetflix.out
.
----------------------------------------------------------------------
Ran 1 test in 0.000s

OK
Name             Stmts   Miss Branch BrPart  Cover   Missing
------------------------------------------------------------
Netflix.py          27      0      4      0   100%
TestNetflix.py      13      0      0      0   100%
------------------------------------------------------------
TOTAL               40      0      4      0   100%

"""
